/* felix2.h
 *
 * Copyright (c) 2011-2014 David J Felix
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef FELIX2_H
	#define FELIX2_H

	#include "felix1.h"
	
	#ifndef FELIX_MACRO_DEPS
	#define FELIX_MACRO_DEPS
		#include <limits.h>
		#include <stdint.h>
		#include <stdlib.h>
		#include <string.h>
		#include <stdbool.h>
	#endif

	typedef struct f2node_ {
		f2_node_ **links; 
		felix1 *estimator;
		uint8_t linkcnt;
	} f2node;
	
	// Felix 2 Estimator
	typedef struct felix2_ {
		f2node_ *head;
		size_t max_size;
	} felix2;
	

	extern inline void destroy_f2e(felix2 *);
	extern _Bool gen_f2e(_Bool *, size_t);
	extern inline felix2* init_f2e(size_t);

#endif
