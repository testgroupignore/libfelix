/* felix1.h
 *
 * Copyright (c) 2011-2014 David J Felix
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef FELIX1_H
	#define FELIX1_H

	#include "felix.h"

	#ifndef FELIX_MACRO_DEPS
	#define FELIX_MACRO_DEPS
		#include <limits.h>
		#include <stdint.h>
		#include <stdlib.h>
		#include <string.h>
		#include <stdbool.h>
	#endif

	// Felix 1 Estimator
	typedef struct felix1_ {
		void *state;
		uint8_t size;
	} felix1;

	extern inline void destroy_f1e(felix1 *);
	extern _Bool gen_f1e(const felix1, _Bool *);
	extern inline _Bool get_f1e_count_neg(const felix1, ulong_t *);
	extern inline _Bool get_f1e_count_pos(const felix1, ulong_t *);
	extern inline _Bool get_f1e_count_tot(const felix1, ulong_t *);
	extern _Bool get_f1e_frac(const felix1, ulong_t *, ulong_t *);
	extern _Bool get_f1e_mode(const felix1, _Bool *);
	extern inline _Bool get_f1e_prob(const felix1, double *);
	extern inline felix1* init_f1e();
	extern _Bool samp_f1e(felix1 *, const _Bool);

#endif
